package com.example.somekotaku.kotakurssreader.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
@Root(name = "channel")
public class Channel {
    @Element(name = "title")
    private String title;
    @Element(name = "link")
    private String link;
    @Element(name = "description")
    private String description;
    @Element(name = "language")
    private String language;
    @ElementList(name = "item", required = true, inline = true)
    public List<RssItem> itemList;

    @Override
    public String toString() {
        return "Channel{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", language='" + language + '\'' +
                ", itemList=" + itemList +
                '}';
    }
}
