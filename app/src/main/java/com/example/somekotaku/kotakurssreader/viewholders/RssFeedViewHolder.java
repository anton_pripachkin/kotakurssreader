package com.example.somekotaku.kotakurssreader.viewholders;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.somekotaku.kotakurssreader.R;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public class RssFeedViewHolder extends RecyclerView.ViewHolder {
    public CardView cardView;
    public ImageView imageView;
    public TextView titleTextView;
    public TextView dateTextView;

    public RssFeedViewHolder(View itemView) {
        super(itemView);

        cardView = (CardView) itemView.findViewById(R.id.card_view);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);
        titleTextView = (TextView) itemView.findViewById(R.id.feedTitle);
        dateTextView = (TextView) itemView.findViewById(R.id.feedDate);
    }
}
