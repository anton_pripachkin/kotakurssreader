package com.example.somekotaku.kotakurssreader.ui.activities;

import android.app.FragmentManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.somekotaku.kotakurssreader.R;
import com.example.somekotaku.kotakurssreader.database.CacheDatabase;
import com.example.somekotaku.kotakurssreader.interfaces.BaseUtilitiesActivity;
import com.example.somekotaku.kotakurssreader.interfaces.CacheRssFeedDatabase;
import com.example.somekotaku.kotakurssreader.utilities.NetworkUtilities;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public abstract class BaseActivity extends AppCompatActivity implements BaseUtilitiesActivity {
    private NetworkUtilities networkUtilities;

    @Override
    public NetworkUtilities getNetworkUtilities() {
        if (networkUtilities == null) {
            networkUtilities = new NetworkUtilities(this);
        }
        return networkUtilities;
    }


    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showInternetErrorToast() {
        Toast.makeText(this, R.string.no_internet, Toast.LENGTH_SHORT).show();
    }

}
