package com.example.somekotaku.kotakurssreader.interfaces;

import com.example.somekotaku.kotakurssreader.models.RSS;

import retrofit.Callback;

/**
 * Created by apripachkin on 10/7/15.
 */
public interface FeedRequestCaller {
    void getLatestFeed(Callback<RSS> callback);
}
