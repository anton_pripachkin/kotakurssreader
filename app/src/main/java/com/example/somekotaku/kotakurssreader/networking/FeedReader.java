package com.example.somekotaku.kotakurssreader.networking;

import com.example.somekotaku.kotakurssreader.interfaces.FeedRequestCaller;
import com.example.somekotaku.kotakurssreader.models.RSS;

import retrofit.Callback;


/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public class FeedReader implements FeedRequestCaller {
    @Override
    public void getLatestFeed(Callback<RSS> callback) {
        FeedServiceProvider feedServiceProvider = FeedServiceProvider.getInstance();
        feedServiceProvider.getService().loadFeed(callback);
    }
}
