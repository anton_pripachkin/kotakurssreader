package com.example.somekotaku.kotakurssreader.interfaces;

/**
 * Created by apripachkin on 10/7/15.
 */
public interface WebFragmentController {
    boolean canReturnToPreviousPage();

    void pressBack();
}
