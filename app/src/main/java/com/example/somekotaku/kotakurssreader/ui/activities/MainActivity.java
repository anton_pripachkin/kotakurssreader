package com.example.somekotaku.kotakurssreader.ui.activities;

import android.app.Fragment;
import android.os.Bundle;

import com.example.somekotaku.kotakurssreader.R;
import com.example.somekotaku.kotakurssreader.interfaces.FeedInteractionCallback;
import com.example.somekotaku.kotakurssreader.interfaces.RssFeedFragmentController;
import com.example.somekotaku.kotakurssreader.interfaces.WebFragmentController;
import com.example.somekotaku.kotakurssreader.models.RssItem;
import com.example.somekotaku.kotakurssreader.ui.fragments.RssFeedFragment;
import com.example.somekotaku.kotakurssreader.ui.fragments.RssFeedListFragment;
import com.example.somekotaku.kotakurssreader.utilities.FragmentHelpUtil;

public class MainActivity extends BaseActivity implements RssFeedFragmentController, FeedInteractionCallback {
    public static final String RSS_FEED_LIST_TAG = RssFeedListFragment.class.getName();
    public static final String RSS_FEED_TAG = RssFeedFragment.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RssFeedListFragment rssFeedListFragment = new RssFeedListFragment();
        FragmentHelpUtil.replaceFragment(getFragmentManager(), rssFeedListFragment,
                R.id.fragment_container, false, RSS_FEED_LIST_TAG);
    }

    @Override
    public void onFeedClicked(RssItem rssItem) {
        if (getNetworkUtilities().isConnectionAvailable()) {
            RssFeedFragment instance = RssFeedFragment.getInstance(rssItem);
            FragmentHelpUtil.addFragment(getFragmentManager(), instance,
                    R.id.fragment_container, true, RSS_FEED_TAG);
        } else {
            showInternetErrorToast();
        }
    }

    @Override
    public void onBackPressed() {
        WebFragmentController webFragment = getWebViewNavigationFragment(RSS_FEED_TAG);
        if (webFragment != null && webFragment.canReturnToPreviousPage()) {
            webFragment.pressBack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public WebFragmentController getWebViewNavigationFragment(String tag) {
        Fragment fragment = FragmentHelpUtil.getFragmentByTag(getFragmentManager(), tag);
        if (fragment instanceof WebFragmentController) {
            return (WebFragmentController) fragment;
        }
        return null;
    }

}
