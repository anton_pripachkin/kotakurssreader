package com.example.somekotaku.kotakurssreader.networking;

import com.example.somekotaku.kotakurssreader.utilities.Constants;
import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.SimpleXMLConverter;


/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public class FeedServiceProvider {
    private static FeedServiceProvider instance;

    private FeedServiceProvider() {

    }

    public static FeedServiceProvider getInstance() {
        if (instance == null) {
            instance = new FeedServiceProvider();
        }
        return instance;
    }

    private RestAdapter getAdapter() {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(Constants.TIMEOUT, TimeUnit.SECONDS);
        RestAdapter adapter = new RestAdapter.Builder()
                .setConverter(new SimpleXMLConverter())
                .setEndpoint(Constants.KOTAKU_URL)
                .setClient(new OkClient(new OkHttpClient()))
                .build();
        return adapter;
    }

    public FeedService getService() {
        return getAdapter().create(FeedService.class);
    }

}
