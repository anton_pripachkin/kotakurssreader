package com.example.somekotaku.kotakurssreader.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.somekotaku.kotakurssreader.R;
import com.example.somekotaku.kotakurssreader.interfaces.FeedInteractionCallback;
import com.example.somekotaku.kotakurssreader.interfaces.ImageLoader;
import com.example.somekotaku.kotakurssreader.models.RssItem;
import com.example.somekotaku.kotakurssreader.utilities.ImgTagParser;
import com.example.somekotaku.kotakurssreader.utilities.PicassoImageLoader;
import com.example.somekotaku.kotakurssreader.utilities.Utilities;
import com.example.somekotaku.kotakurssreader.viewholders.RssFeedViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public class RssFeedAdapter extends RecyclerView.Adapter<RssFeedViewHolder> {
    private List<RssItem> feedList = new ArrayList<>();
    private FeedInteractionCallback feedInteractionCallback;
    private ImageLoader imageLoader;

    public RssFeedAdapter(List<RssItem> feedList, Context context,
                          FeedInteractionCallback feedInteractionCallback) {
        checkFeedList(feedList);
        imageLoader = new PicassoImageLoader(context);
        this.feedInteractionCallback = feedInteractionCallback;
    }

    public RssItem removeItem(int position) {
        RssItem model = feedList.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, RssItem model) {
        feedList.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        RssItem model = feedList.remove(fromPosition);
        feedList.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void animateTo(List<RssItem> models) {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    @Override
    public RssFeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View recycleView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_element, parent, false);
        return new RssFeedViewHolder(recycleView);
    }

    @Override
    public void onBindViewHolder(RssFeedViewHolder holder, int position) {
        final RssItem rssItem = feedList.get(position);

        String title = rssItem.getTitle();
        if (!Utilities.shouldHideView(title, holder.titleTextView)) {
            holder.titleTextView.setText(title);
        }

        String pubDate = rssItem.getPubDate();
        if (!Utilities.shouldHideView(pubDate, holder.dateTextView)) {
            holder.dateTextView.setText(pubDate);
        }

        String imageUrl = ImgTagParser.getImageUrl(rssItem.getDescription());
        if (!Utilities.shouldHideView(imageUrl, holder.imageView)) {
            imageLoader.loadImage(imageUrl, holder.imageView);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feedInteractionCallback != null) {
                    feedInteractionCallback.onFeedClicked(rssItem);
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return feedList.size();
    }

    private void checkFeedList(List<RssItem> feedList) {
        if (feedList != null && !(feedList.isEmpty())) {
            this.feedList.clear();
            this.feedList.addAll(feedList);
        }
    }

    private void applyAndAnimateAdditions(List<RssItem> newModels) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final RssItem model = newModels.get(i);
            if (!feedList.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<RssItem> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final RssItem model = newModels.get(toPosition);
            final int fromPosition = feedList.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    private void applyAndAnimateRemovals(List<RssItem> newModels) {
        for (int i = feedList.size() - 1; i >= 0; i--) {
            final RssItem model = feedList.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }
}
