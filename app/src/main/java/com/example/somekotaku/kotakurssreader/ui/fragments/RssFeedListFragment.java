package com.example.somekotaku.kotakurssreader.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.baoyz.widget.PullRefreshLayout;
import com.example.somekotaku.kotakurssreader.R;
import com.example.somekotaku.kotakurssreader.adapters.RssFeedAdapter;
import com.example.somekotaku.kotakurssreader.interfaces.BaseUtilitiesActivity;
import com.example.somekotaku.kotakurssreader.interfaces.FeedInteractionCallback;
import com.example.somekotaku.kotakurssreader.interfaces.FeedRequestCaller;
import com.example.somekotaku.kotakurssreader.models.RSS;
import com.example.somekotaku.kotakurssreader.models.RssItem;
import com.example.somekotaku.kotakurssreader.networking.FeedReader;
import com.example.somekotaku.kotakurssreader.utilities.RssFeedCacher;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RssFeedListFragment extends Fragment implements TextWatcher {
    private RecyclerView feedsRecyclerView;
    private PullRefreshLayout pullRefreshLayout;
    private FeedInteractionCallback feedInteractionCallback;
    private BaseUtilitiesActivity baseUtilitiesActivity;
    private RssFeedCacher rssFeedCacher;
    private RssFeedAdapter rssFeedAdapter;
    private EditText editText;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof FeedInteractionCallback
                && activity instanceof BaseUtilitiesActivity) {
            feedInteractionCallback = (FeedInteractionCallback) activity;
            baseUtilitiesActivity = (BaseUtilitiesActivity) activity;
        } else {
            throw new IllegalStateException("Must implement " + FeedInteractionCallback.class.getName() +
                    " and " + BaseUtilitiesActivity.class.getName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_main, container, false);
        initializeViews(inflate);
        return inflate;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        refreshView();
    }

    private void refreshView() {
        if (baseUtilitiesActivity.getNetworkUtilities().isConnectionAvailable()) {
            makeFeedRequest();
        } else if (rssFeedCacher.hasCachedData()) {
            showCachedData();
        } else {
            baseUtilitiesActivity.showInternetErrorToast();
            pullRefreshLayout.setRefreshing(false);
        }

    }

    private void showCachedData() {
        setFeedAdapter(rssFeedCacher.getCachedData());
        pullRefreshLayout.setRefreshing(false);
    }

    private void setFeedAdapter(List<RssItem> rssItems) {
        rssFeedAdapter = new RssFeedAdapter(rssItems,
                getActivity(),
                feedInteractionCallback);
        feedsRecyclerView.setAdapter(rssFeedAdapter);
    }

    private void makeFeedRequest() {
        FeedRequestCaller feedReader = new FeedReader();
        feedReader.getLatestFeed(new Callback<RSS>() {
            @Override
            public void success(RSS rss, Response response) {
                List<RssItem> feedList = rss.getChannel().itemList;
                setFeedAdapter(feedList);
                rssFeedCacher.cacheData(feedList);
                pullRefreshLayout.setRefreshing(false);
            }

            @Override
            public void failure(RetrofitError error) {
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                        .setTitle(R.string.error)
                        .setMessage(error.getMessage())
                        .setPositiveButton("OK", null)
                        .create();
                alertDialog.show();
                pullRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void initializeViews(View inflate) {
        feedsRecyclerView = (RecyclerView) inflate.findViewById(R.id.feeds_recycler_view);
        rssFeedCacher = new RssFeedCacher(getActivity());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        feedsRecyclerView.setLayoutManager(linearLayoutManager);
        editText = (EditText) inflate.findViewById(R.id.editText);
        editText.addTextChangedListener(this);
        pullRefreshLayout = (PullRefreshLayout) inflate.findViewById(R.id.pullRefreshLayout);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshView();
            }
        });
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence query, int start, int before, int count) {
        if (rssFeedAdapter != null) {
            List<RssItem> feedList = rssFeedCacher.getCachedData();
            if (!feedList.isEmpty()) {
                List<RssItem> filteredList = filter(feedList, query.toString());
                rssFeedAdapter.animateTo(filteredList);
                feedsRecyclerView.scrollToPosition(0);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private List<RssItem> filter(List<RssItem> models, String query) {
        query = query.toLowerCase();
        List<RssItem> filteredModelList = new ArrayList<>();
        for (RssItem model : models) {
            String text = model.getTitle().toLowerCase();
            if (text.contains(query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }
}
