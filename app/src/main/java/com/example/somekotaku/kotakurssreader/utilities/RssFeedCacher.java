package com.example.somekotaku.kotakurssreader.utilities;


import android.content.Context;

import com.example.somekotaku.kotakurssreader.database.CacheDatabase;
import com.example.somekotaku.kotakurssreader.interfaces.CacheRssFeedDatabase;
import com.example.somekotaku.kotakurssreader.models.RssItem;

import java.util.List;

/**
 * Created by apripachkin on 10/9/15.
 */
public class RssFeedCacher {
    private CacheRssFeedDatabase cacheRssFeedDatabase;

    public RssFeedCacher(Context context) {
        cacheRssFeedDatabase = new CacheDatabase(context);
    }

    public void cacheData(List<RssItem> rssItemList) {
        if (!cacheRssFeedDatabase.hasData()) {
            cacheRssFeedDatabase.addFeedsToDatabase(rssItemList);
        } else {
            List<RssItem> databaseList = cacheRssFeedDatabase.readValues();
            if (!databaseList.containsAll(rssItemList)) {
                cacheRssFeedDatabase.deleteAllData();
                cacheRssFeedDatabase.addFeedsToDatabase(rssItemList);
            }
        }
    }

    public boolean hasCachedData() {
        return cacheRssFeedDatabase.hasData();
    }

    public List<RssItem> getCachedData() {
        return cacheRssFeedDatabase.readValues();
    }
}
