package com.example.somekotaku.kotakurssreader.interfaces;

import com.example.somekotaku.kotakurssreader.models.RssItem;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public interface FeedInteractionCallback {
    void onFeedClicked(RssItem rssItem);

    void onBackPressed();
}
