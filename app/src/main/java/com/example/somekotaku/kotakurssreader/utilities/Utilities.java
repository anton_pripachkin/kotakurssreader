package com.example.somekotaku.kotakurssreader.utilities;

import android.text.TextUtils;
import android.view.View;

/**
 * Created by Anton Pripachkin on 08.10.2015.
 */
public class Utilities {
    public static boolean isEmptyString(String text) {
        return TextUtils.isEmpty(text);
    }

    public static boolean shouldHideView(String text, View view) {
        boolean result = false;
        if (isEmptyString(text)) {
            hideView(view);
            result = true;
        }
        return result;
    }

    private static void hideView(View view) {
        view.setVisibility(View.GONE);
    }
}
