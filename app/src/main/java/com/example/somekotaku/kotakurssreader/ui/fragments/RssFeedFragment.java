package com.example.somekotaku.kotakurssreader.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.example.somekotaku.kotakurssreader.R;
import com.example.somekotaku.kotakurssreader.interfaces.RssFeedFragmentController;
import com.example.somekotaku.kotakurssreader.interfaces.WebFragmentController;
import com.example.somekotaku.kotakurssreader.models.RssItem;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public class RssFeedFragment extends Fragment implements WebFragmentController {
    private static final String RSS_FEED_FRAGMENT_URL = "rssFeedFragmentUrl";
    private WebView webviewFeed;
    private ProgressBar progressBar;
    private String rssItemUrl;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof RssFeedFragmentController)) {
            throw new IllegalStateException("Must Implement " + RssFeedFragmentController.class.getName());
        }
    }

    public static RssFeedFragment getInstance(RssItem rssItem) {
        Bundle args = new Bundle();
        args.putString(RSS_FEED_FRAGMENT_URL, rssItem.getLink());
        RssFeedFragment rssFeedFragment = new RssFeedFragment();
        rssFeedFragment.setArguments(args);
        return rssFeedFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View initializedView = inflater.inflate(R.layout.feed_webview_fragment, container, false);
        webviewFeed = (WebView) initializedView.findViewById(R.id.webviewFeed);
        progressBar = (ProgressBar) initializedView.findViewById(R.id.progressBar);
        initializeWebView();
        rssItemUrl = getArguments().getString(RSS_FEED_FRAGMENT_URL);
        webviewFeed.loadUrl(rssItemUrl);
        return initializedView;
    }

    private void initializeWebView() {
        if (webviewFeed != null) {
            WebSettings settings = webviewFeed.getSettings();
            settings.setJavaScriptEnabled(true);
            webviewFeed.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    progressBar.setVisibility(View.GONE);
                }

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    progressBar.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    public boolean canReturnToPreviousPage() {
        return webviewFeed.canGoBack();
    }

    @Override
    public void pressBack() {
        webviewFeed.goBack();
    }
}
