package com.example.somekotaku.kotakurssreader.utilities;

import android.content.Context;
import android.widget.ImageView;

import com.example.somekotaku.kotakurssreader.interfaces.ImageLoader;
import com.squareup.picasso.Picasso;

/**
 * Created by Anton Pripachkin on 08.10.2015.
 */
public class PicassoImageLoader implements ImageLoader {
    private Picasso picasso;

    public PicassoImageLoader(Context context) {
        picasso = Picasso.with(context);
    }

    @Override
    public void loadImage(String url, ImageView view) {
        picasso.load(url).into(view);
    }
}
