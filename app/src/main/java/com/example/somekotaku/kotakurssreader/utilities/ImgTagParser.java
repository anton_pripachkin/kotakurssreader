package com.example.somekotaku.kotakurssreader.utilities;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public class ImgTagParser {

    public static String getImageUrl(String source) {
        String imgRegex = "<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>";
        Pattern pattern = Pattern.compile(imgRegex);
        Matcher matcher = pattern.matcher(source);
        String result = null;
        if (matcher.find()) {
            result = matcher.group(1);
        }
        return result;
    }

}
