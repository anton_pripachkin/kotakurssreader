package com.example.somekotaku.kotakurssreader.interfaces;

import android.widget.ImageView;

/**
 * Created by Anton Pripachkin on 08.10.2015.
 */
public interface ImageLoader {
    void loadImage(String url, ImageView view);
}
