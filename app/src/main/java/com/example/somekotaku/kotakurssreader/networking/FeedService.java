package com.example.somekotaku.kotakurssreader.networking;

import com.example.somekotaku.kotakurssreader.models.RSS;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public interface FeedService {
    @GET("/")
    void loadFeed(Callback<RSS> callback);
}
