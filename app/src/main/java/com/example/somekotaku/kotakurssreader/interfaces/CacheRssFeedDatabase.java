package com.example.somekotaku.kotakurssreader.interfaces;

import com.example.somekotaku.kotakurssreader.models.RssItem;

import java.util.List;

/**
 * Created by apripachkin on 10/8/15.
 */
public interface CacheRssFeedDatabase {
    void addFeedsToDatabase(List<RssItem> feedlist);

    void deleteAllData();

    List<RssItem> readValues();

    boolean hasData();
}
