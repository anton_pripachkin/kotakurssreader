package com.example.somekotaku.kotakurssreader.utilities;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public final class Constants {
    private Constants() {
    }

    public static final int TIMEOUT = 4;
    public static final String KOTAKU_URL = "http://kotaku.com/rss";
}
