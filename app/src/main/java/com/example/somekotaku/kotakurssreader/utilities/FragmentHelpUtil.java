package com.example.somekotaku.kotakurssreader.utilities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.text.TextUtils;

/**
 * Created by apripachkin on 10/8/15.
 */
public class FragmentHelpUtil {
    public static void replaceFragment(FragmentManager fragmentManager, Fragment fragmentToReplace, int hostView, boolean addToBackStack, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (TextUtils.isEmpty(tag)) {
            fragmentTransaction.replace(hostView, fragmentToReplace);
        } else {
            fragmentTransaction.replace(hostView, fragmentToReplace, tag);
        }
        addToBackStack(fragmentToReplace, addToBackStack, fragmentTransaction);
        fragmentTransaction.commit();
    }

    private static void addToBackStack(Fragment fragmentToReplace, boolean addToBackStack, FragmentTransaction fragmentTransaction) {
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragmentToReplace.getClass().getName());
        }
    }

    public static void addFragment(FragmentManager fragmentManager, Fragment fragmentToAdd, int hostView, boolean addToBackStack, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (TextUtils.isEmpty(tag)) {
            fragmentTransaction.add(hostView, fragmentToAdd);
        } else {
            fragmentTransaction.add(hostView, fragmentToAdd, tag);
        }
        addToBackStack(fragmentToAdd, addToBackStack, fragmentTransaction);
        fragmentTransaction.commit();
    }

    public static Fragment getFragmentByTag(FragmentManager fragmentManager, String tag) {
        return fragmentManager.findFragmentByTag(tag);
    }
}
