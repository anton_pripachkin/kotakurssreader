package com.example.somekotaku.kotakurssreader.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.somekotaku.kotakurssreader.interfaces.CacheRssFeedDatabase;
import com.example.somekotaku.kotakurssreader.models.RssItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apripachkin on 10/7/15.
 */
public class CacheDatabase extends SQLiteOpenHelper implements CacheRssFeedDatabase {

    private static final String TABLE_NAME = "cachedEntriesTable";
    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String LINK = "link";
    private static final String DESCRIPTION = "description";
    private static final String DATE = "pubDate";
    private static final String DB_NAME = "cacheDB";

    public CacheDatabase(Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME + " ( " + ID + " integer primary key autoincrement, " +
                "" + TITLE + " text," +
                " " + LINK + " text," +
                " " + DESCRIPTION + " text," +
                " " + DATE + " text );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public void addFeedsToDatabase(List<RssItem> feedlist) {
        for (RssItem item :
                feedlist) {
            insertValue(item);
        }
    }

    private void insertValue(RssItem rssItem) {
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, rssItem.getTitle());
        contentValues.put(LINK, rssItem.getLink());
        contentValues.put(DESCRIPTION, rssItem.getDescription());
        contentValues.put(DATE, rssItem.getPubDate());
        database.insert(TABLE_NAME, null, contentValues);
    }

    @Override
    public void deleteAllData() {
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL("delete from " + TABLE_NAME);
    }

    @Override
    public List<RssItem> readValues() {
        List<RssItem> resultList = new ArrayList<>();
        SQLiteDatabase database = getWritableDatabase();
        Cursor query = database.query(TABLE_NAME, null, null, null, null, null, null);
        if (query.moveToFirst()) {
            int titleIndex = query.getColumnIndex(TITLE);
            int linkIndex = query.getColumnIndex(LINK);
            int descriptionIndex = query.getColumnIndex(DESCRIPTION);
            int dateIndex = query.getColumnIndex(DATE);
            do {
                addRssItem(resultList, query, titleIndex, linkIndex, descriptionIndex, dateIndex);
            } while (query.moveToNext());
        }
        query.close();
        return resultList;
    }

    @Override
    public boolean hasData() {
        Cursor query = null;
        try {
            SQLiteDatabase database = getWritableDatabase();
            query = database.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            return query != null && (query.getCount() > 0);
        } finally {
            if (query != null) {
                query.close();
            }
        }
    }


    private void addRssItem(List<RssItem> resultList, Cursor query, int titleIndex,
                            int linkIndex, int descriptionIndex, int dateIndex) {
        String title = query.getString(titleIndex);
        String link = query.getString(linkIndex);
        String description = query.getString(descriptionIndex);
        String date = query.getString(dateIndex);
        RssItem rssItem = new RssItem();
        rssItem.setTitle(title);
        rssItem.setLink(link);
        rssItem.setDescription(description);
        rssItem.setPubDate(date);
        resultList.add(rssItem);
    }
}
