package com.example.somekotaku.kotakurssreader.interfaces;


/**
 * Created by apripachkin on 10/7/15.
 */
public interface RssFeedFragmentController {
    WebFragmentController getWebViewNavigationFragment(String tag);
}
