package com.example.somekotaku.kotakurssreader.interfaces;

import com.example.somekotaku.kotakurssreader.utilities.NetworkUtilities;

/**
 * Created by Anton Pripachkin on 06.10.2015.
 */
public interface BaseUtilitiesActivity {
    NetworkUtilities getNetworkUtilities();

    void showInternetErrorToast();
}
